define([], function Script()
{
 	return {
 		"title":"The King of Chicago",
		"actors": [
			{
				"name":"Bob",
				"health":100
			}
		],
		"scenes": [
			{
				"id":"start",
				"description":"This is the first scene.",
				"script": [
					{"Bob":"Hello."},
					{"Frank":"Hello to you."}
				],
				"actions": [
					{
						"text":"Hit bob",
						"effects": [{
							"actor":"Bob",
							"health":-1
						}],
						"nextScene" : "bobHit"
					}
				]
			},
			{
				"id":"bobHit",
				"description":"You hit Bob",
				"script": [
					{"Bob":"Ouch!"}
				],
				"actions": []
			},
			{
				"id":"bobAngry",
				"description":"Bob is mad at you",
				"requirements": {
					"Bob" : {
						"health":"<100"
					}
				},
				"script": [
					{"Bob":"I'm mad at you!"}
				]
			}
		]
    };
});
