requirejs(["core/dramaton","jquery"], function(dramaton) {
	start = function()
	{
		try
		{
			dramaton.initialize();
			$("div.title").html(dramaton.getTitle());
			showScene();
		}
		catch(err)
		{
			console.log(err.stack);
			window.alert(err);
		}
	}

	load = function(url) 
	{
 		$.getJSON(url, onLoad);
 	}

	showScene = function()
	{
		$(".script").empty();
		$(".actions").empty();

		var scene = dramaton.currentScene;

		if(scene==null)
		{
			// Just restart.
			window.alert("Reached end");
			//this.start();
		}
		else
		{
			scene.lines.forEach(function(line) {
				printLine(line);
			});

			if(scene.actions && scene.actions.length>0)
			{
				var actionIndex = 0;
				scene.actions.forEach(function(action) {
					addAction(action, actionIndex++);
				});
			}
			else
			{
				addDefaultAction();
			}
		}
	}

	printLine = function(line)
	{
		var vals = Object.values(line);
		$(".script").append("<p>" + line.actor + " : " +line.text + "</p>");
	}

	performAction = function(actionIndex)
	{
		try
		{
			dramaton.performAction(actionIndex);
			showScene();
		}
		catch(err)
		{
			console.log(err.stack);
			window.alert(err);
		}
	}

	addAction = function(action, actionIndex)
	{
		$(".actions").append("<div class='action' onClick='performAction(" + actionIndex + ");'>" + action.text + "</div>");
	}

	addDefaultAction = function()
	{
		$(".actions").append("<div class='action' onClick='performAction(null);'>Continue</div>");
	}

	start();
});