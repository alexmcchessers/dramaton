define(["text!script.txt"], function ScriptParser(scriptTxt)
{
 	return {
 		lineIndex: 0,
 		lines:  [],
 		script: {},

 		parse : function()
 		{
 			this.lines = scriptTxt.match(/[^\r\n]+/g);
 			this.script.title = this.lines[0];
 			this.script.scenes = [];
 			this.script.actors = [];

 			if(this.lines.length>1)
 			{
	 			this.lineIndex = 1;
	 			for(this.lineIndex = 1; this.lineIndex < this.lines.length; this.lineIndex++)
	 			{
	 				var splitLine = this.splitCurrentLine();

	 				if(splitLine!=null)
	 				{
	 					var prefix = splitLine[0].toLowerCase();
		 				if(prefix == "scene")
		 				{
		 					this.parseScene(splitLine);
		 				}
		 			}
	 			}
	 		}

 			return this.script;
 		},

 		splitCurrentLine: function()
 		{
		 	var line = this.lines[this.lineIndex].trim();
		 	var splitLine = null;
			if(line.length>0)
			{
				splitLine = line.split(":");
				if(splitLine.length<2)
				{
					throw "Cannot parse line \"" + line + "\"";
				}
				else
				{
					for(var i = 0; i < splitLine.length; i++)
					{
						splitLine[i] = splitLine[i].trim();
					}
				}
			}
			return splitLine;
 		},

 		parseScene: function(firstSplitLine)
 		{
 			var scene = {};
 			scene.lines = [];
 			scene.actions = [];

 			scene.name=firstSplitLine[1].trim();

 			for(++this.lineIndex; this.lineIndex < this.lines.length; this.lineIndex++)
 			{
 				var splitLine = this.splitCurrentLine();

 				if(splitLine!=null)
 				{
 					var prefix = splitLine[0].toLowerCase();
	 				switch(prefix)
	 				{
	 					case "action":
	 						scene.actions.push(this.parseAction(splitLine));
	 						break

	 					default:
	 						var line = {
	 							actor: splitLine[0],
	 							text: splitLine[1]
	 						}
	 						scene.lines.push(line);
	 				}
	 			}
 			}

 			this.script.scenes.push(scene);
 		},

 		parseAction: function(firstSplitLine)
 		{
 			var action = {};
 			action.effects = [];
 			action.text = firstSplitLine[1];

 			for(++this.lineIndex; this.lineIndex < this.lines.length; this.lineIndex++)
 			{
 				var splitLine = this.splitCurrentLine();

 				if(splitLine!=null)
 				{
	 				switch(splitLine[0])
	 				{
	 					case "effect":
	 						action.effects.push
	 						break

	 					default:
	 						
	 				}
	 			}
 			}
 			return action;
 		}
 	}
 });