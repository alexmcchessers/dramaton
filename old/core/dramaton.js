define(["core/scriptParser"], function Dramatron(parser)
{
 	return {

 		currentScene : null,

        initialize: function()
        {
            this.script = parser.parse();
            this.setCurrentScene(this.script.scenes[0]);  
        },

        setCurrentScene: function(scene)
        {
            this.currentScene = scene;
        },

        getTitle: function () 
        {
            return this.script.title;
        },

        performAction: function(actionIndex)
        {
            var nextScene = null;

            if(actionIndex!=null)
            {
                var action = this.currentScene.actions[actionIndex];
                action.effects.forEach(function(effect){
                    this.applyEffect(effect);
                },this);

                if(action.nextScene)
                {
                    nextScene = this.unvisitedSceneMap[action.nextScene];
                    if(!nextScene)
                    {
                        throw "Scene " + action.nextScene + " not found!";
                    }
                }
            }

            if(nextScene==null)
            {
                nextScene = this.pickNextScene();
            }

            if(nextScene!=null)
            {
                this.setCurrentScene(nextScene);
            }
            else
            {
                this.currentScene = null;
            }

        	return this.currentScene;
        },

        applyEffect: function(effect)
        {
            var actor = this.gameState.actorMap[effect.actor];

            if(!actor)
            {
                throw "Actor " + effect.actor + " not found.";
            }

            Object.keys(effect).forEach(function(property) {
                if(property!="actor")
                {
                    actor[property]+=effect[property];
                }
            });
        },

        pickNextScene: function()
        {
            var validScenes = [];
            // Evaluate all unvisited scenes
            Object.values(this.unvisitedSceneMap).forEach(function(scene){
                if(scene.requirements)
                {
                    if(this.evaluateRequirements(scene.requirements))
                    {
                         validScenes.push(scene);
                    }
                }
            }, this)

            // Pick a random scene
            var nextScene = null;
            if(validScenes.length>0)
            {
                var sceneIndex = Math.round(Math.random()*validScenes.length-1);
                nextScene = validScenes[sceneIndex];
            }
            return nextScene;
        },

        evaluateRequirements: function(reqs)
        {
            // Each key is an actor
            var reqsMet = true;
            var actorIds = Object.keys(reqs);
            for(var i =0; i < actorIds.length; i++)
            {
                if(!this.gameState.actorMap[actorIds[i]])
                {
                    reqsMet = false;
                    throw "Actor " + actorIds[i] + " not found.";
                }
                reqsMet = this.evaluateRequirementsForActor(this.gameState.actorMap[actorIds[i]], reqs[actorIds[i]]);
                if(!reqsMet)
                {
                    break;
                }
            }
            return reqsMet;
        },

        evaluateRequirementsForActor: function(actor, reqs)
        {
            var reqsMet = true;
            var fields = Object.keys(reqs);
            for(var i =0; i < fields.length; i++)
            {
                var toEval = "actor." + fields[i];
                if(!isNaN(parseInt(reqs[fields[i]])))
                {
                    toEval+="=";
                }
                toEval+=reqs[fields[i]];
                reqsMet = eval(toEval);
                if(!reqsMet)
                {
                    break;
                }
            } 

            return reqsMet;
        }
    };
});