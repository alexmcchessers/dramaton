# vue-project-template

## Project initialize
```
git clone --depth=1 http://git.bvops.net/scm/vue/vue-project-template.git [project name]
```
If you want to start with a clean git history (the above command truncates history to the last commit of the template project)
run the follow commands.
```
cd [project name]
```
```
rm -rf .git
```
```
git init
```

## Setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Remove demo code
```
rm -rf src/demo
```
Find and delete the few references to demo code in the following files:
 src/App.vue,
 src/main.ts,
 src/router/index.ts,
 src/store/index.ts

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
