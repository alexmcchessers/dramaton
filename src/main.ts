import 'material-design-icons-iconfont/dist/material-design-icons.css'

import Vue from 'vue';
import '@/plugins/axios';
import '@/plugins/vuetify';
import App from '@/App.vue';
import Player from '@/views/Player.vue';
import router from './router';
import Vuetify from 'vuetify'

Vue.config.productionTip = false;

Vue.use(Vuetify, {
  iconfont: 'mdi' // 'md' || 'mdi' || 'fa' || 'fa4'
})

// handle page reloads
const app: Vue = new Vue({
  el: '#app',
  router,
  components: {
    Player,
  },
  render: h => h(App),
});
