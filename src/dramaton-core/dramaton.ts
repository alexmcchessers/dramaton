import { Script } from './model/script';
import { ScriptParser } from './scriptparser';

export class Dramaton {

    private script:Script|undefined = undefined;

    constructor(scriptTxt:string) {
       this.script = ScriptParser.parse(scriptTxt);
    }

    public getScript() : Script|undefined {
        return this.script;
    }
}

export default Dramaton;
