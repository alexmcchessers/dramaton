import { Event } from './event';

export interface Action {
  description:string,
  effects:Event[],
}

export default Action;