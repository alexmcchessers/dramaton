import { Plot } from './plot';

export interface Script {
  name:string,
  author?:string,
  plots:Plot[]
}

export default Script;
