import { Scene } from './scene';

export interface Plot {
  id:string,
  currentSceneIndex:number,
  scenes:Scene[]
}

export default Plot;
