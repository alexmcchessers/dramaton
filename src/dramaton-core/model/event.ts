import  { Operator } from './operator';

export interface Event {
  description:string,
  object:string,
  property?:string,
  operator?:Operator,
  operand?:string
}

export default Event;
