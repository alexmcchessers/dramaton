import { Event } from './event';
import { Action } from './action';

export interface Scene {
    id: string,
    title: string,
    location?: string,
    entryEvents?: Event[],
    playerActions?: Action[]
  }

export default Scene;

