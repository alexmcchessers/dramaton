import { Script } from './model/script';
import { Plot } from './model/plot';
import { Scene } from './model/scene';
import { Event } from './model/event';
import { Action } from './model/action';

export abstract class ScriptParser {
    private static lineNumber:number = 0;
    
    public static parse(scriptTxt:string): Script {
        let lines:string[]|null = scriptTxt.match(/[^\r\n]+/g);

        let scriptName:string = 'UNDEFINED';
        let author:string|undefined = undefined;
        let plots:Plot[] = [];

        if(lines) {
            for(ScriptParser.lineNumber = 0; ScriptParser.lineNumber < lines.length; ScriptParser.lineNumber++) {
                let sections:string[] = ScriptParser.splitLine(lines[ScriptParser.lineNumber]);
                if(sections.length>=2) {

                    if(sections[0] === "name") {
                        scriptName = sections[1];
                    } else if(sections[0] === "author") {
                        author = sections[1];
                    } else if(sections[0] === "scene") {
                        ScriptParser.parseScene(plots, sections[1]);
                    }
                }
            }
        }

        let thisScript:Script = {
            name: scriptName,
            author: author,
            plots: plots
        };

        return thisScript;
    }

    private static parseScene(plots:Plot[], sceneName:string) {
        let scene:Scene = {
            id: sceneName,
            title: ''
        }
    }

    private static splitLine(line:string) : string[]
    {
        let splitLine:string[] = [];
        line = line.trim();
        if(line.length > 0 && !line.startsWith("#")) {
            splitLine = line.split(":");
            for(let i:number = 0; i < splitLine.length; i++) {
                splitLine[i] = splitLine[i].trim();
            }
            splitLine[0] = splitLine[0].toLowerCase();
        }
            
        return splitLine;
    }

    
}
