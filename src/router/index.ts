import Vue from 'vue';
import Router from 'vue-router';
import Player from '@/views/Player.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'player',
      component: Player,
    }
  ],
  scrollBehavior(to, from, savedPosition) {
    if (to.hash) {
      return {
        selector: to.hash,
        offset: { x: 0, y: 75 },
      };
    }
    return {
      x: 0,
      y: 0,
    };
  },
});
